package com.airborneRF.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.airborneRF.domain.NumbersData;
import com.airborneRF.service.PersistenceServiceImpl;

@RestController
@RequestMapping(value = "/numbersData")
public class NumbersDataController {
	
	@Autowired
	private PersistenceServiceImpl persistenceServiceImpl;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/getNumbersData", method = RequestMethod.GET)
	public ResponseEntity<List<NumbersData>> getNumbersData() {
		return ResponseEntity.ok().body(persistenceServiceImpl.getNumbersData());
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/addNumbersData", method = RequestMethod.POST)
	public ResponseEntity<NumbersData> addNumbersData(@RequestBody NumbersData numbersData) throws Exception { 
		return ResponseEntity.ok().body(persistenceServiceImpl.addOrUpdateNumbersData(numbersData));
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/updateNumbersData", method = RequestMethod.PUT)
	public ResponseEntity<NumbersData> updateNumbersData(@RequestBody NumbersData numbersData) throws Exception { 
		return ResponseEntity.ok().body(persistenceServiceImpl.addOrUpdateNumbersData(numbersData));
	}

}

package com.airborneRF.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.airborneRF.model.NumbersDataEntity;

public interface NumbersDataDao extends JpaRepository<NumbersDataEntity, Long> {

}

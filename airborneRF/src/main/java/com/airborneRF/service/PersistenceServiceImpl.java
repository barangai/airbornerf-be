package com.airborneRF.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airborneRF.domain.NumbersData;
import com.airborneRF.model.NumbersDataEntity;
import com.airborneRF.repository.NumbersDataDao;
import com.airborneRF.util.CPUSortingUtils;
import com.airborneRF.util.DomainToEntityConverter;
import com.airborneRF.util.EntityToDomainConverter;

@Transactional
@Service
public class PersistenceServiceImpl {
	
	@Autowired
	private NumbersDataDao numbersDataDao;
	
	public List<NumbersData> getNumbersData() {
		List<NumbersDataEntity> numbersEntities = numbersDataDao.findAll();
		List<NumbersData> numbersDomains = new ArrayList<NumbersData>();
		
		if(numbersEntities != null && numbersEntities.size() > 0) {
			for(final NumbersDataEntity it : numbersEntities) {
				numbersDomains.add(EntityToDomainConverter.convertFrom(it));
			}
		}
		
		return numbersDomains;
	}
	
	public NumbersData addOrUpdateNumbersData(NumbersData numbersData) throws Exception {
		NumbersDataEntity entity = new NumbersDataEntity();
		try {
			if(numbersData.getId() != null) {
				CPUSortingUtils.sortNumbersOnTheCpu(numbersData.getNumbers());
			}
			entity = numbersDataDao.saveAndFlush(DomainToEntityConverter.convertTo(numbersData));
			
		} catch (Exception e) {
			throw new Exception("Error saving/updating numbers data");
		}
		return EntityToDomainConverter.convertFrom(entity);
	}

}

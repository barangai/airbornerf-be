package com.airborneRF.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CPUSortingUtils {
	
	public static void sortNumbersOnTheCpu(String unsortedNumbers) throws IOException {
		Process process = new ProcessBuilder("C:\\Personal\\AirborneRF CPU Computation\\AirborneRF CPU Computation\\out\\build\\x64-Debug\\CMakeTarget.exe", "8", "5", "12", "4", "1").start();
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		// Read the output from the command
		System.out.println("Here is the standard output of the command:\n");
		String s = null;
		while ((s = stdInput.readLine()) != null) {
		    System.out.println(s);
		}

		// Read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
		    System.out.println(s);
		}
	}

}

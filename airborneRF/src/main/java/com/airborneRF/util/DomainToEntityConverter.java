package com.airborneRF.util;

import com.airborneRF.domain.NumbersData;
import com.airborneRF.model.NumbersDataEntity;

public class DomainToEntityConverter {
	
	public static NumbersDataEntity convertTo(NumbersData numbersData) {
		NumbersDataEntity entity = new NumbersDataEntity();
		
		if(numbersData.getId() != null) {
			entity.setId(numbersData.getId());
		}
		entity.setNumbers(numbersData.getNumbers());
		entity.setCreationDate(numbersData.getCreationDate());
		entity.setUpdatedDate(numbersData.getUpdatedDate());
		
		return entity;
	}

}

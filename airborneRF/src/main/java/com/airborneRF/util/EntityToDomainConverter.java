package com.airborneRF.util;

import com.airborneRF.domain.NumbersData;
import com.airborneRF.model.NumbersDataEntity;

public class EntityToDomainConverter {
	
	public static NumbersData convertFrom(NumbersDataEntity entity) {
		NumbersData domain = new NumbersData();
		domain.setId(entity.getId());
		domain.setNumbers(entity.getNumbers());
		domain.setCreationDate(entity.getCreationDate());
		domain.setUpdatedDate(entity.getUpdatedDate());
		return domain;
	}

}

package com.airborneRF.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GPUSortingUtils {
	
	public static void sortNumbersOnTheGpu(String unsortedNumbers) throws IOException {
//		ArrayList<String> elephantList = (ArrayList<String>)Arrays.asList(unsortedNumbers.split(","));
		
		final Process process = new ProcessBuilder("C:\\Personal\\AirborneRF GPU Computation\\AirborneRF GPU Computation\\x64\\Debug\\cuda-10.1-template-2019.exe", 
				"8", "5", "13", "4", "1")
				.start();
		final BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		final BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		// Read the output from the command
		System.out.println("Here is the standard output of the command:\n");
		String s = null;
		while ((s = stdInput.readLine()) != null) {
		    System.out.println(s);
		}

		// Read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
		    System.out.println(s);
		}
	}

}

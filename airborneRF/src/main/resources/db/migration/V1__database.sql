CREATE TABLE numbers_data (
	id INTEGER(11) not null,
	numbers VARCHAR(50) not null,
	creationDate DATE NOT NULL,
	updatedDate DATE NOT NULL);
	
ALTER TABLE numbers_data
  ADD CONSTRAINT pk_numbers_data_id PRIMARY KEY (id);